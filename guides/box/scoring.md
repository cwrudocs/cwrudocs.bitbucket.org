<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>quiz scoring matrix v1</title>
<link rel="canonical" href="http://cwrudocs.bitbucket.org/guides/box/scoring.html" />
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<style>
	table { table-layout: fixed; }
	.quiz {
  		table-layout: fixed;
  		width: 100%;
		}

	/* Column widths are based on these cells */
	.row-outcomeID { width: 5%; }
	.row-answerText { width: 60%; word-wrap: break-word;}

	h2 { padding-top: 30px; }
	h3 { padding-top: 20px; }
</style>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
<p>&nbsp;</p>
<pre>
                     _                
                    | |               
 _ __ _ __  _ __ ___| |_ _______ _ __ 
| '__| '_ \| '__/ _ \ __|_  / _ \ '__|
| |  | |_) | | |  __/ |_ / /  __/ |   
|_|  | .__/|_|  \___|\__/___\___|_|   
     | |                              
     |_|


Scroll down to the bottom of this page to add comments ...

NOTES: I'll add in the remainder of weighting ranks this evening. 

</pre>        

<h3>Legend</h3>
<p>Scale (least appropriate to most)<br>
Weight {0,1,3,7,9}</p>

<p>Result Index<br>
B1 = Google Drive<br>
B2 = Box<br>
B3 = OnBase<br>
B4 = Panassus (research computing)<br>
B5 = Nexsan (research computing)<br>
B6 = FCSRE (medical research)<br>
B7 = Amazon S3 (external other)<br>
B8 = Celerra (network share)</p>


<p>&nbsp;</p>

<table id="Q1" class="table table-condensed table-hover quiz">
<caption>Q1: Choose your own adventure. Pick one option that best describes your role.</caption>
	<thead>
		<tr>
			<!-- <th class="row-answerID">&nbsp;</th> --><th class="row-answerText">&nbsp;</th>
			<th class="row-outcomeID">B1</th><th class="row-outcomeID">B2</th><th class="row-outcomeID">B3</th><th class="row-outcomeID">B4</th><th class="row-outcomeID">B5</th><th class="row-outcomeID">B6</th><th class="row-outcomeID">B7</th><th class="row-outcomeID">B8</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<!-- <th>A1</th> -->
			<td>[01] I'm a student</td>
			<td>9</td><td>9</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A2</th> -->
			<td>[02] I'm an independent researcher</td>
			<td>3</td><td>9</td><td>0</td><td>7</td><td>9</td><td>3</td><td>1</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A3</th> -->
			<td>[03] I'm faculty or staff (no research)</td>
			<td>9</td><td>9</td><td>7</td><td>0</td><td>0</td><td>0</td><td>3</td><td>3</td>
		</tr>
		<tr>
			<!-- <th>A4</th> -->
			<td>[04] I represent a department or workgroup</td>
			<td>0</td><td>7</td><td>9</td><td>1</td><td>1</td><td>1</td><td>3</td><td>7</td>
		</tr>
		<tr>
			<!-- <th>A5</th> -->
			<td>[05] I represent a research project team</td>
			<td>1</td><td>9</td><td>0</td><td>9</td><td>3</td><td>9</td><td>1</td><td>0</td>
		</tr>
	</tbody>
</table>


<p>&nbsp;</p>
<table id="Q2" class="table table-condensed table-hover quiz">
<caption>Q2: Imagine that a free of cost storage option (Google Drive, Box) will not meet your needs. Describe your willingness and capability to pay for an alternative in terms of fruit.</caption>
	<thead>
		<tr>
			<!-- <th class="row-answerID">&nbsp;</th> --><th class="row-answerText">&nbsp;</th>
			<th class="row-outcomeID">B1</th><th class="row-outcomeID">B2</th><th class="row-outcomeID">B3</th><th class="row-outcomeID">B4</th><th class="row-outcomeID">B5</th><th class="row-outcomeID">B6</th><th class="row-outcomeID">B7</th><th class="row-outcomeID">B8</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<!-- <th>A6</th> -->
			<td>[06] 1 banana</td>
			<td>9</td><td>9</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A7</th> -->
			<td>[07] Refreshing concord grapes, picked from my own vineyard</td>
			<td>0</td><td>0</td><td>9</td><td>9</td><td>9</td><td>9</td><td>9</td><td>9</td>
		</tr>
		<tr>
			<!-- <th>A8</th> -->
			<td>[08] Who cares? Charge my Speed Type / pcard</td>
			<td>1</td><td>1</td><td>9</td><td>3</td><td>3</td><td>3</td><td>9</td><td>9</td>
		</tr>
		<tr>
			<!-- <th>A9</th> -->
			<td>[09] A recently rediscovered strain of Heirloom tomato (I've received a grant to study contemporary seed saving.)</td>
			<td>0</td><td>7</td><td>0</td><td>9</td><td>3</td><td>9</td><td>1</td><td>1</td>
		</tr>
		<tr>
			<!-- <th>A10</th> -->
			<td>[10] We're working on a grant application now. Just think, heirloom tomatoes enhanced with animal genetic material!</td>
			<td>0</td><td>7</td><td>0</td><td>9</td><td>3</td><td>9</td><td>1</td><td>1</td>
		</tr>
	</tbody>
</table>


<p>&nbsp;</p>
<table id="Q3" class="table table-condensed table-hover quiz">
<caption>Q3: Consider your data and workflow. Would you sacrifice a degree of privacy in order to better track the flow of information?<br>
<br>
CAVEAT: Although we're committed to reasonable assurances of privacy, there are times when it's necessary to look inside the box and check on your cat (oops! sorry Schr&ouml;dinger :( )</caption>
	<thead>
		<tr>
			<!-- <th class="row-answerID">&nbsp;</th> --><th class="row-answerText">&nbsp;</th>
			<th class="row-outcomeID">B1</th><th class="row-outcomeID">B2</th><th class="row-outcomeID">B3</th><th class="row-outcomeID">B4</th><th class="row-outcomeID">B5</th><th class="row-outcomeID">B6</th><th class="row-outcomeID">B7</th><th class="row-outcomeID">B8</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<!-- <th>A11</th> -->
			<td>[11] Deal. Our department conducts periodic security and compliance audits of our information systems.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A12</th> -->
			<td>[12] No deal. My personal data is my own.</td>
			<td>9</td><td>7</td><td>0</td><td>7</td><td>3</td><td>3</td><td>7</td><td>1</td>
		</tr>
		<tr>
			<!-- <th>A13</th> -->
			<td>[13] We need access control. We're deeply concerned about both protecting privacy and preserving data integrity.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A14</th> -->
			<td>[14] We're interested in publishing data broadly and want to understand how it's being used in a community of practice.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A15</th> -->
			<td>[15] Our IP is valuable. We need to protect against data loss and leakage.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
	</tbody>
</table>


<p>&nbsp;</p>
<table id="Q4" class="table table-condensed table-hover quiz">
<caption>Q4: Do you collect PHI?</caption>
	<thead>
		<tr>
			<!-- <th class="row-answerID">&nbsp;</th> --><th class="row-answerText">&nbsp;</th>
			<th class="row-outcomeID">B1</th><th class="row-outcomeID">B2</th><th class="row-outcomeID">B3</th><th class="row-outcomeID">B4</th><th class="row-outcomeID">B5</th><th class="row-outcomeID">B6</th><th class="row-outcomeID">B7</th><th class="row-outcomeID">B8</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<!-- <th>A16</th> -->
			<td>[16] Yes. Lots!</td>
			<td>0</td><td>9</td><td>1</td><td>0</td><td>0</td><td>9</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A17</th> -->
			<td>[17] We keep other kinds of sensitive information.</td>
			<td>0</td><td>9</td><td>7</td><td>3</td><td>1</td><td>7</td><td>0</td><td>0</td>
		</tr>
	</tbody>
</table>


<p>&nbsp;</p>
<table id="Q5" class="table table-condensed table-hover quiz">
<caption>Q5: Do you need to share your data, and if so, with whom?</caption>
	<thead>
		<tr>
			<!-- <th class="row-answerID">&nbsp;</th> --><th class="row-answerText">&nbsp;</th>
			<th class="row-outcomeID">B1</th><th class="row-outcomeID">B2</th><th class="row-outcomeID">B3</th><th class="row-outcomeID">B4</th><th class="row-outcomeID">B5</th><th class="row-outcomeID">B6</th><th class="row-outcomeID">B7</th><th class="row-outcomeID">B8</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<!-- <th>A18</th> -->
			<td>[18] I have cat pictures.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A19</th> -->
			<td>[19] Yes. With colleagues outside of CWRU</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A20</th> -->
			<td>[20] Yes. With co-workers or colleagues at CWRU</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A21</th> -->
			<td>[21] No, and I'd rather not.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
	</tbody>
</table>


<p>&nbsp;</p>
<table id="Q6" class="table table-condensed table-hover quiz">
<caption>Q6: Describe a frustrating aspect of your daily work.</caption>
	<thead>
		<tr>
			<!-- <th class="row-answerID">&nbsp;</th> --><th class="row-answerText">&nbsp;</th>
			<th class="row-outcomeID">B1</th><th class="row-outcomeID">B2</th><th class="row-outcomeID">B3</th><th class="row-outcomeID">B4</th><th class="row-outcomeID">B5</th><th class="row-outcomeID">B6</th><th class="row-outcomeID">B7</th><th class="row-outcomeID">B8</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<!-- <th>A22</th> -->
			<td>[22] We transfer data from one system to another by hand.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A23</th> -->
			<td>[23] We scan all our paperwork and send it for approval by email.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A24</th> -->
			<td>[24] Ramen using the hotplate :(</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A25</th> -->
			<td>[25] We don't have many rote processes and need to act on our shared data with an arbitrary set of applications. It'd be great if we could track changes more closely.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
	</tbody>
</table>


<p>&nbsp;</p>
<table id="Q7" class="table table-condensed table-hover quiz">
<caption>Q7: Do you need to access your data from Starbucks?</caption>
	<thead>
		<tr>
			<!-- <th class="row-answerID">&nbsp;</th> --><th class="row-answerText">&nbsp;</th>
			<th class="row-outcomeID">B1</th><th class="row-outcomeID">B2</th><th class="row-outcomeID">B3</th><th class="row-outcomeID">B4</th><th class="row-outcomeID">B5</th><th class="row-outcomeID">B6</th><th class="row-outcomeID">B7</th><th class="row-outcomeID">B8</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<!-- <th>A26</th> -->
			<td>[26] No. We also a secure physical facility.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A27</th> -->
			<td>[27] Of course. I should be able to access data from anywhere.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A28</th> -->
			<td>[28] It's complicated. My data has to be stored in 'Ohio'.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A28</th> -->
			<td>[29] I object to the use of Starbucks in this example.</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
	</tbody>
</table>


<p>&nbsp;</p>
<table id="Q8" class="table table-condensed table-hover quiz">
<caption>Q8: Finally, how soon do you need storage set up?</caption>
	<thead>
		<tr>
			<!-- <th class="row-answerID">&nbsp;</th> --><th class="row-answerText">&nbsp;</th>
			<th class="row-outcomeID">B1</th><th class="row-outcomeID">B2</th><th class="row-outcomeID">B3</th><th class="row-outcomeID">B4</th><th class="row-outcomeID">B5</th><th class="row-outcomeID">B6</th><th class="row-outcomeID">B7</th><th class="row-outcomeID">B8</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<!-- <th>A29</th> -->
			<td>[29] Yesterday</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A30</th> -->
			<td>[30] In the next month or two</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		<tr>
			<!-- <th>A31</th> -->
			<td>[31] We're not in a rush</td>
			<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
		</tr>
		</tbody>
</table>

<!-- <div id="results">
desc: DRIVE
desc: BOX
desc: OnBase
desc: Panassus
desc: Nexsan
desc: FCSRE
desc: S3
desc: Celerra
</div> -->

<div id="disqus_thread"></div>
<script>
    var disqus_config = function () {
        this.page.url = 'http://cwrudocs.bitbucket.org/guides/box/scoring.html';
        this.page.identifier = 'cwrudocs.bitbucket.org';
    };
    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        
        s.src = '//cwrudocs.disqus.com/embed.js';
        
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
</div>
<script id="dsq-count-scr" src="//cwrudocs.disqus.com/count.js" async></script>
</div>
</body>
</html>