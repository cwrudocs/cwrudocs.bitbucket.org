 

Research Applications | Research Archive Storage

**Keywords**

# Research Computing
 Research Storage Archive

ITS Research Data Archive provides a low cost mechanism to store inactive data. Using the first RESTful interface to tape, **Research Storage Archive** provides storage and client interaction via REST, allowing faculty and staff researchers to move large data sets easily to and from the research archive.

### How-to + Training

[Request service][]
 [Service options][]
 [Option costs][Service options]

### [Report an Outage][]

* * * * *

## Tools for Resolution

### Required Information

-   Name
-   Contact information (preferred and alternate)
-   CWRU Network ID
-   Case Email Address
-   Problem or Request description

### Resolver Groups

**Research Computing** is available Mon.-Fri., 8 a.m - 5 p.m., EST. for general enquiry. For non-escalated issues, troubleshooting will begin within 4 hours.

## How to Escalate

An escalation may be requested by an end user when he or she determines that a critical issue has not been acknowledged properly, remains unresolved for an unacceptable period of time or is severe enough that it requires atypical handling.

Escalation should not be confused with assignment to a resolver group. An escalated issue is typically brought to the attention of management within a second-level support group(s).

### Who to Call

**Research Storage Archive** is not considered a critical service.

Escalation is available during business hours. Escalation is not available during university closures, including holidays. For escalations due to outage outside of planned maintenance windows, support teams will engage within 15 minutes.

**Sanjaya Gajurel**, [(216) 368-5717][] (office)
 **Em Dragowsky**, [(216) 368-0082][] (office)
 **Mike Warfe**, [(216) 368-5647][] (office)

* * * * *

### Request service

[back to top][]

To start service, direct the user to contact Research Computing by email at [its-cluster-admins@case.edu][].

1.  Gather resolution information and enter it into the ticket.
2.  Provide user with overview of service options and costs.
3.  For additional consultation, assign ticket to **Research Computing**.

### Options and Costs

[back to top][]

Four service options are available. To set a future-dated consultation direct the user to contact Research Computing by email at [its-cluster-admins@case.edu][].

1.  Gather resolution information and enter it into the ticket.
2.  Provide user with overview of service options and costs.
3.  For additional consultation, assign ticket to **Research Computing**.

|Option|Description|Cost|
|:-----|:----------|:---|
|Archive data to one set of tapes|Data is archived to tape. Once archival is completed the tapes are ejected and the lab keeps the tapes.|Price of tapes at market cost (LTO-6 2.5TB \$70.00)|
|Archive data to two sets of tapes|Data is archived to two sets of tape. Once archival is completed both sets of tapes are ejected. One set is kept with ITS and the other is kept by the lab.|Price of tapes at market cost (LTO-6 2.5TB \$70.00)|
|Archive data to two sets of tape with offsite vaulting|Data is archived to two sets of tape. Once archival is completed both sets of tapes are ejected. One set is kept with ITS and the other is kept by our offsite facility.|Price of tapes at market cost (LTO-6 2.5TB \$70.00)|
|Active archive|Tape is kept online and in the Spectra library.|Price of tapes at market cost (LTO-6 2.5TB \$70.00) and cost of slot rental (\$200 per slot utilized per year)|

### Report an Outage

[back to top][]

1.  Gather resolution information and enter it into the ticket.
2.  Outages should be assigned to the **Research Computing** resolver group, who will determine the scope of issue and appropriate next steps.

* * * * *

© 2016, Case Western Reserve University

  [Request service]: #newservice
  [Service options]: #options
  [Report an Outage]: #outage
  [(216) 368-5717]: tel:2163685717
  [(216) 368-0082]: tel:2163680082
  [(216) 368-5647]: tel:2163685647
  [back to top]: #
  [its-cluster-admins@case.edu]: mailto:its-cluster-admins@case.edu
