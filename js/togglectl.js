function cwruits_togglectl(node) {
  var section=node.parentNode;
  while (true) {
    section=section.nextSibling;
    if (!section) return null;
    if (section.nodeType==Node.ELEMENT_NODE) break;
  }
  if (node.innerHTML=='[-]') {
    $(section).slideUp();
    node.innerHTML='[+]';
  } else {
    $(section).slideDown();
    node.innerHTML='[-]';
  }
  return null;
}