# CWRU UTech Documentation home #

This is the home for service desk scripts, quick reference guides, and other instructional material propagated by CWRU UTech while in development (or during rework). The master branch is the gold copy from which all other material should be derived or refer to.